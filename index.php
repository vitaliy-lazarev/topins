<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <title>Topins</title>
    <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animations.css">
    <!-- <link rel="stylesheet" href="css/demo.css"> -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="block-parallax hidden-xs" id='intro'></div>
    <a name="top" id='top'></a>
    <div class="wrapper">
    <div class="section s1">
        <div class="header-bg inner">
            <div class="container block-margin">
                <div class="row">
                    <div class="col-sm-2 col-xs-12 animatedParent">
                        <div class="block-seti animated bounceInLeft">
                            <a class="google">
                            </a>
                            <a class="facebook">
                            </a>
                            <a class="twitter">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2 col-xs-offset-0 col-xs-12 animatedParent block-logo-header">
                        <a class="block-logo animated bounceInDown" href='#'>
                        </a>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2 col-xs-offset-0 animatedParent block-hidden">
                        <div class="block-contacts animated bounceInRight">
                            <div class="telefon">
                                +49 6052 9182965
                            </div>
                            <div class="mail">
                                Hubertus.rick@blackcurrant-group.de
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 block-margin-left-none subMenu hidden-xs">
                    <ul class='menu inner hidden-xs'>
                                <li><a href="#s1" class="subNavBtn">Zuhause</a></li>
                                <li><a href="#s2" class="subNavBtn">Über Topins</a></li>
                                <li><a href="#s3" class="subNavBtn">Aktion Topins</a></li>
                                <li><a href="#s4" class="subNavBtn">Topins Leben</a></li>
                                <li><a href="#s5" class="subNavBtn">Eigenschaften Topins</a></li>
                            </ul>
                        
                </div>


                <div class="dropdown subMenu hidden-sm hidden-md hidden-lg">
                <div class="dropdown-menu block-rest-drop" role='menu' aria-labelledby='dLabel'>
                    <ul class='inner block-menu-bg'>
                                <li><a href="#s1" class="subNavBtn">Zuhause</a></li>
                                <li><a href="#s2" class="subNavBtn">Über Topins</a></li>
                                <li><a href="#s3" class="subNavBtn">Aktion Topins</a></li>
                                <li><a href="#s4" class="subNavBtn">Topins Leben</a></li>
                                <li><a href="#s5" class="subNavBtn">Eigenschaften Topins</a></li>
                            </ul>
                            <div class="block-seti">
                            <a class="google">
                            </a>
                            <a class="facebook">
                            </a>
                            <a class="twitter">
                            </a>
                        </div>
                    </div>
                <a class='block-img-menu' data-toggle="dropdown" href="#"></a> <p class="block-menu-color" data-toggle="dropdown">Menü</p>
                        
                </div>


                                
            </div>
            </div>
            <div class="container block-text-margin animatedParent">
                <div class="row">
                    <div class="col-sm-7 block-animate-left animated bounceInUp block-rotait-children">
                        <div class="block-logo-toppins">
                        </div>
                        <h3>Gewichtskontrolle ganz natürlich</h3>
                        <h2>Die Diät-Revolution</h2>
                        <h4>Toppins — der Keks, der Satt macht!</h4>
                        <p>Toppins ist der neue, leckere Keks gegen Heißhunger-Attacken für eine gesunde Gewichtskontrolle. Toppins bremst das Hungergefühl und sorgt dafür, dass man nicht mehr isst, als man sollte. Dann funktioniert es auch mit der Diät!</p>
                    </div>
                    <div class="col-sm-4 col-sm-offset-1 block-animate-right animated bounceInDown block-img-children">
                        <div class="block-bg-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div><!-- s1 -->
        <!-- End Header-bg-->
        <div class="section s2">
        <div class="container animatedParent inner block-text-center">
            <div class="row">
                <div class="col-sm-12">
                    <p>Wie funktioniert Toppins?</p>
                </div>
            </div>
        </div>
        <div class="block-parent-four">
            <div class="block-line hidden-xs">
                <img src="image/line-bg.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-xs-10 col-sm-offset-1 col-xs-offset-2 animatedParent block-crums-text">
                        <div class="block-one block-after-one animated bounceInLeft">
                            <h3>Gebäck mit Topinambur</h3>
                            <p>Sie haben stets ein gutes, sattes Gefühl und die bekannten Heißhungerattacken bleiben aus. Toppins ist damit eine gute Ergänzung für eine ausgewogenen Ernährung sowie ausreichend Bewegung.</p>
                            <img src="image/cookies.png" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-10 col-xs-offset-2 col-sm-offset-2 animatedParent block-crockery-text">
                        <div class="block-one block-after-two animated bounceInRight">
                            <h3>Toppins, der Keks, der satt macht</h3>
                            <p>Toppins zählt nicht als Mahlzeitenersatz, sondern kann als gesunder und sättigender Snack zwischen den Hauptmahlzeiten genossen werden.</p>
                            <img src="image/tarelca.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row block-margin-parent">
                    <div class="col-sm-5 col-xs-10 col-xs-offset-2 col-sm-offset-1 animatedParent block-vesi-text">
                        <div class="block-one block-after-tree animated bounceInLeft">
                            <h3>Gewichtskontrolle ganz natürlich</h3>
                            <p>Mit 18% Topinambur ist das Gebäck reich an dem angenehm sättigenden Ballaststoff Inulin und hilft Ihnen bei der Kontrolle des Körpergewichts und ist eine ideale Ergänzung zu Ihrer Diät.</p>
                            <img src="image/vesi.png" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 block-text-vesi col-xs-10 col-xs-offset-2 col-sm-offset-2 animatedParent block-dumbbells-text">
                        <div class="block-one block-after-four animated bounceInRight">
                            <h3>Die Diät Revolution</h3>
                            <p>Toppins ist dazu gedacht Ihre Diät zu unterstützen. Die appetithemmende Wirkung von TOPPINS entsteht durch den Einsatz von den sättigenden und kalorienarmen Wirkstoffen der Topinambur Pflanze,  die in neu patentierter Form kombiniert wurden.</p>
                            <img src="image/ganteli.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block-parent-table">
            <div class="container">
                <div class="row animatedParent" data-sequence="200">
                    <div class="col-sm-2 animated growIn" data-id='1'>
                        <img src="image/yagodu.png" alt="">
                    </div>
                    <div class="col-sm-7 animated growIn" data-id='2'>
                        <div class="block-text-paragraph">
                            <p>Dadurch wird Toppins neben den appetithemmenden Eigenschaften zu einem Produkt, welches direkt Ihre Gesundheit beeinflussen und zu einem gesünderen Lebensstil beitragen kann.</p>
                        </div>
                    </div>
                    <div class="col-sm-3 animated growIn block-table-padding" data-id='3'>
                        <p class='table-name'>100 g enthält:</p>
                        <table>
                            <tr>
                                <td>Fett</td>
                                <td>15 g</td>
                            </tr>
                            <tr>
                                <td>Eiweiß</td>
                                <td>0,7 g</td>
                            </tr>
                            <tr>
                                <td>Kohlenhydrate</td>
                                <td>64 g</td>
                            </tr>
                            <tr>
                                <td>Kcal</td>
                                <td>441 kcal</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="section s3">
        <div class="container block-text-margin-parent inner">
            <div class="row">
                <div class="col-sm-12 animatedParent">
                    <div class="block-header">
                        <p>Wie hemmt Toppins den Appetit?</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 animatedParent">
                    <p class='block-content-left animated bounceInLeft'>Das Geheimnis der einzigartigen Wirkungsweise von Toppins liegt in den Eigenschaften der Topinamburknolle. Topinambur ist reich an Ballaststoffen und enthält Inulin. Neben den sättigenden Eigenschaften ist Inulin ein wahrer Wunderstoff. Dadurch wird Toppins neben den sättigenden Eigenschaften zu einem Produkt, welches direkt Ihre Gesundheit beeinflussen und zu einem gesünderen Lebensstil beitragen kann.
                        <br>
                        <br> Inulin ist für verschiedene Dinge bekannt:
                        <br>
                        <br> - Inulin ist präbiotisch. Durch die einzigartige &nbsp;&nbsp;Struktur von Inulin ist es dem menschlichen Körper &nbsp;&nbsp;nicht möglich es selbst zu verdauen.</p>
                </div>
                <div class="col-sm-4 animatedParent">
                    <img class='block-content-center' src="image/artishok.png" alt="">
                </div>
                <div class="col-sm-4 animatedParent">
                    <p class='block-content-right animated bounceInRight'>Durch die einzigartige Struktur von Inulin ist es dem menschlichen Körper nicht möglich es selbst zu verdauen. Der Körper braucht dazu Bakterien die in natürlicher form im Dickdarm vorkommen. Durch den Verzehr von Inulinhaltigen Produkten wie Toppins werden diese Bakterien gezielt angeregt. Somit trägt Inulin zu einer Gesunden Darmflora bei.
                        <br>
                        <br> - Dadurch, dass Inulin die Darmflora unterstützt &nbsp;&nbsp;indem es das Wachstum von gesunden Bakterien &nbsp;&nbsp;im Darm begünstigt, hilft der Verzehr von Inulin &nbsp;&nbsp;indirekt dem Immunsystem.
                        <br>
                        <br> - Studien haben gezeigt, dass Inulin die Aufnahme &nbsp;&nbsp;von Mineralstoffen fördert.</p>
                </div>
            </div>
        </div>
        <div class="block-parent-table block-text-block-margin">
            <div class="container">
                <div class="row animatedParent" data-sequence="200">
                    <div class="col-sm-2 block-img-margin animated growIn" data-id="1">
                        <p class='block-style-triangle'>Lassen Sie sich Toppins
                            <br> schmecken</p>
                    </div>
                    <div class="col-sm-3 block-img-margin block-text-margin-ipad col-sm-offset-1 animated growIn" data-id="2">
                        <p class='block-circle'>Toppins ist präbiotisch, dadurch fördert Ihr Immunsystem.</p>
                    </div>
                    <div class="col-sm-3 block-img-margin animated growIn" data-id="3">
                        <p class='block-piramida'>Die empfohlene Verzehrmenge von 5 Keksen täglich sollte nicht überschritten werden.</p>
                    </div>
                    <div class="col-sm-3 animated growIn" data-id="4">
                        <p class='block-leaves'>Bei der Herstellung von TOPPINS wurde besonderen Wert auf die Verwendung von natürlichen Zutaten gelegt.</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="section s4">
        <div class="container block-ben-parent">
            <div class="row">
                <div class="col-sm-12 animatedParent">
                    <div class="block-header-ben">
                        <p>wo kann man Toppins BENUTZEN?</p>
                        <div class="row">
                            <div class="col-sm-8 block-text-description block-media-parent col-sm-offset-2 col-xs-offset-0">
                                <p>Durch die praktische Keksform kann man Toppins in allen Lebensbereichen verwenden. Da keine weitere Zubereitung nötig ist sondern man einfach nur einen Keks isst, kann man Toppins überall mit hin nehmen.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row animatedParent block-text-animated">
                <div class="col-sm-12 col-lg-8 block-text-import-margin animated lightSpeedInLeft">
                    <div class="block-img-left">
                        <img src="image/foto_one.jpg" alt="">
                    </div>
                    <div class="block-text-right">
                        <h3>Im Büro</h3>
                        <p>Bei der Arbeit kann es öfters vorkommen, dass man auf einmal Hunger bekommt und es noch nicht Zeit für die Mittagspause ist. In diesem Fall kann man Toppins essen, damit man nicht unnötig zwischen den Mahlzeiten nascht.</p>
                    </div>
                </div>
            </div>
            <div class="row animatedParent block-text-animated">
                <div class="col-sm-12 col-lg-8 block-text-import-margin col-lg-offset-4 animated lightSpeedInRight">
                    <div class="block-img-right">
                        <img src="image/foto_two.jpg" alt="">
                    </div>
                    <div class="block-text-left">
                        <h3>Zuhause</h3>
                        <p>Sie arbeiten viel im Haushalt und gönnen sich zwischendurch eine wohlverdiente Pause. Oft ist diese Pause verbunden mit einem kleinen Snack, der natürlich ungünstig für eine Diät ist. Hier können Sie anstatt eines Stücks Schokolade einfach einen Toppins Keks essen und der Hunger ist weg.</p>
                    </div>
                </div>
            </div>
            <div class="row animatedParent block-text-animated">
                <div class="col-sm-12 col-lg-8 block-text-import-margin animated lightSpeedInLeft">
                    <div class="block-img-left">
                        <img src="image/foto_tree.jpg" alt="">
                    </div>
                    <div class="block-text-right">
                        <h3>Unterwegs</h3>
                        <p>Wenn Sie viel im Auto unterwegs sind kommt es öfters mal vor, dass man auf der Fahrt etwas nascht ums ich die lange Fahrt etwas zu versüßen. Hier kann Toppins helfen den Hunger und Appetit zu hemmen damit Sie Ihre Diät weiter erfolgreich meistern können.</p>
                    </div>
                </div>
            </div>
            <div class="row animatedParent block-text-animated">
                <div class="col-sm-12 col-lg-8 col-lg-offset-4 animated lightSpeedInRight">
                    <div class="block-img-right">
                        <img src="image/foto_four.jpg" alt="">
                    </div>
                    <div class="block-text-left block-important-text">
                        <h3>Während der Freizeit</h3>
                        <p>Am Wochenende gibt es immer wieder die Situation, dass man zwischendurch Hunger bekommt. Man ist zum Beispiel zum Grillen eingeladen, aber das Grillen ist erst in 2 Stunden. Um die Zeit zu überbrücken und nicht unnötig Süßigkeiten zu sich zu nehmen und damit Ihre Diät zu gefährden, können sie einfach einen Toppins Keks essen.</p>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <div class="section s5">
            <div class="container inner">
            <div class="row block-muss">
                <div class="col-sm-12 animatedParent">
                    <div class="block-header-ben">
                        <p>Muss man bei der Verzehr von Toppins etwas beachten?</p>
                        <div class="row">
                            <div class="col-sm-6 block-text-description animated bounceInLeft">
                                <p>Toppins besteht aus 100% natürlichen Inhaltsstoffen und enthält keine Chemie. Zu beachten gibt es daher wenig. Trotzdem gibt es einige Sachen zu beachten: Topinambur kann bei 5% der Bevölkerung zu Blähungen führen. Aus diesem Grund sollten Sie bei dem Verzehr von Toppins darauf achten, wie die Verdauung auf Toppins reagiert.</p>
                            </div>
                            <div class="col-sm-6 block-text-description animated bounceInRight">
                                <p>Sollten Sie das Gefühl haben, dass Sie verstärkt Blähungen haben, empfehlen wir die Tagesdosis von Toppins auf 5 zu begrenzen. Allergiker sollten darauf achten, dass durch die Produktion Toppins Spuren von Erdnüssen, Schalenfrüchten, Sesam und Soja enthalten kann.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="container">
            <div class="row block-kann">
                <div class="col-sm-12 animatedParent">
                    <div class="block-header-ben">
                        <p>Wo kann man Toppins kaufen?</p>
                    </div>
                    <div class="row animatedParent" data-sequence="200">
                        <div class="col-sm-4 block-standorte animated growIn" data-id="1">
                            <h2>Standorte</h2>
                            <p>Sie können Toppins in der Apotheke Ihres Vertrauens kaufen. Sollte Ihr Apotheker kein Toppins auf Lager haben, kann er Toppins bei den Großhändlern GeHe und Sanacorp bestellen.</p>
                        </div>
                        <div class="col-sm-4 block-impressum animated growIn" data-id="2">
                            <h2>Impressum</h2>
                            <p>Blackcurrant Group GmbH & Co.KG Villbacher Straße 47
                                <br> 63619 Bad Orb
                                <br>
                                <br> Telefon: +49 6052 9182965
                                <br> Telefax: +49 6052 9182969
                                <br>
                                <br> Geschäftsführer: Hubertus Rick</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 block-hanau animated growIn" data-id="3">
                            <p>Registergericht: Amtsgericht Hanau
                                <br> Registernummer: HRA 92066 Hanau
                                <br> Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 236 809 674
                                <br>
                                <br> Hubertus.rick@blackcurrant-group.de
                                <a href='#'>www.toppins.info</a></p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <a href="#top" id="toTop" class='up'></a>
            <div class="container">
            <div class="row block-text-footer">
                <footer class='animatedParent'>
                    <div class="col-sm-2 animated bounceInLeft">
                        <div class="logo-footer">
                            <a href='#'><img src="image/logo-footer.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-offset-8 animated bounceInRight block-seti-right">
                        <div class="block-seti-footer">
                            <a href="#" class='footer-google'></a>
                            <a href="#" class='footer-facebook'></a>
                            <a href="#" class='footer-twitter'></a>
                        </div>
                    </div>
                    <div class="row block-footer-copy">
                        <div class="col-sm-2 block-left-footer animated bounceInLeft">
                            <div class="copy">
                                <p>© 2015 Toppins</p>
                            </div>
                        </div>
                        <div class="col-sm-3 col-sm-offset-7 block-right-footer animated bounceInRight">
                            <p class='telefon'>+49 6052 9182965</p>
                            <p class='mail'>Hubertus.rick@blackcurrant-group.de</p>
                        </div>
                    </div>
                </footer>
            </div>
            </div>
            </div>
        </div><!-- End Block-ben-parent -->
    </div><!-- End Wrapper -->

    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.up').on('click', function(){
            //Сохраняем значение атрибута href в переменной:
            var target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top}, 1500);
            return false;
            });
        });
    </script>
    <script src="javascript/css3-animate-it.js"></script>
    <script src="javascript/bootstrap.min.js"></script>
    <script src="javascript/jquery.smint.js"></script>

    <!-- <script src="javascript/jquery.parallax-1.1.3.js"></script> -->
    <script>
    $(document).ready(function() {

         $('.subMenu').smint({
        'scrollSpeed' : 500
    });

    

    //parallax
    // $('#intro').parallax("100%", 0.5);




        if (screen.width <= '600') {
            $('.menu li').css('margin-right', '33px');
            $('.block-logo-header').css({
                'position': 'relative',
                'left': '-5%'
            });
            $('.block-bg-img').css({
                'top': '-130px',
                'right': '421px',
                'background-size': '80%'
            });
            $('.block-parent-table .block-text-paragraph').css({
                'margin-top': '15px'
            });
            $('.block-media-parent').css({
                'margin-left': '18%'
            });
            $('.block-img-right').css({
                'float': 'right'
            });
            $('.block-img-left').css({
                'max-width': '100%',
                'float': 'none'
            });
            $('.block-text-left').css({
                'float': 'left',
                'max-width': '100%'
            });
            $('.block-parallax').css({
                'min-height': '7240px'
            });
        }
             
    });

</script>
</body>

</html>
